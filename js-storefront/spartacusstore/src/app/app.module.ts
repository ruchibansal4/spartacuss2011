import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { translations, translationChunksConfig } from '@spartacus/assets';

import { OccConfig, provideDefaultConfig, provideConfig } from '@spartacus/core';
import { environment } from './../environments/environment';
import { B2bStorefrontModule, defaultB2bOccConfig } from '@spartacus/setup';
import { AdministrationRootModule } from '@spartacus/organization/administration/root';
import { organizationTranslations } from '@spartacus/organization/administration/assets';
import { organizationTranslationChunksConfig } from '@spartacus/organization/administration/assets';
import { OrderApprovalRootModule } from '@spartacus/organization/order-approval/root';
import { orderApprovalTranslations } from '@spartacus/organization/order-approval/assets';
import { orderApprovalTranslationChunksConfig } from '@spartacus/organization/order-approval/assets';

const occConfig: OccConfig = { backend: { occ: {} } };

// only provide the `occ.baseUrl` key if it is explicitly configured, otherwise the value of
// <meta name="occ-backend-base-url" > is ignored.
// This in turn breaks the call to the API aspect in public cloud environments
if (environment.occBaseUrl) {
  occConfig.backend.occ.baseUrl = environment.occBaseUrl;
}
if (environment.prefix) {
  occConfig.backend.occ.prefix = environment.prefix;
}
else {
  occConfig.backend.occ.prefix = '/occ/v2/';
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    B2bStorefrontModule.withConfig({
      featureModules: {
        organizationOrderApproval: {
        module: () => import('@spartacus/organization/order-approval').then(
          (m) => m.OrderApprovalModule
        ),
        },
        organizationAdministration: {
          module: () => import('@spartacus/organization/administration').then(
          (m) => m.AdministrationModule
        ),
        },
      },
      backend: occConfig.backend,
      context: {
        urlParameters: ['baseSite', 'language', 'currency'],
        baseSite: ['electronics-spa', 'powertools-spa'],
        currency: ['USD', 'GBP',]
      },
      i18n: {
        resources: translations,
        chunks: translationChunksConfig,
        fallbackLang: 'en'
      },
      features: {
        level: '2.0'
      }
    }),
    BrowserTransferStateModule,
    AdministrationRootModule,
    OrderApprovalRootModule
  ],
  providers: [provideDefaultConfig(defaultB2bOccConfig),
    provideConfig({
      i18n: {
        resources: organizationTranslations,
        chunks: organizationTranslationChunksConfig,
      },
    }),
    
    provideConfig({
      i18n: {
        resources: orderApprovalTranslations,
        chunks: orderApprovalTranslationChunksConfig,
      },
    })],
  bootstrap: [AppComponent]
})
export class AppModule { }
